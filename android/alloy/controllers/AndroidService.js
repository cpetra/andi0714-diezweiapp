function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "AndroidService";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    exports.destroy = function() {};
    _.extend($, $.__views);
    if (Ti.App.Properties.hasProperty("notificationCount")) {
        alert("notification count else");
        Ti.App.Properties.removeProperty("notificationCount");
        var activity = Ti.Android.currentActivity;
        var intent = Ti.Android.createIntent({
            action: Ti.Android.ACTION_MAIN,
            url: "index.js",
            flags: Ti.Android.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Ti.Android.FLAG_ACTIVITY_SINGLE_TOP
        });
        intent.addCategory(Titanium.Android.CATEGORY_LAUNCHER);
        var pending = Ti.Android.createPendingIntent({
            activity: activity,
            intent: intent,
            type: Ti.Android.PENDING_INTENT_FOR_ACTIVITY,
            flags: Ti.Android.FLAG_ACTIVITY_NO_HISTORY
        });
        var notification = Ti.Android.createNotification({
            contentIntent: pending,
            contentTitle: "Test",
            contentText: "test",
            tickerText: "This is a test",
            when: new Date().getTime(),
            icon: Ti.App.Android.R.drawable.appicon,
            flags: Titanium.Android.ACTION_DEFAULT | Titanium.Android.FLAG_AUTO_CANCEL | Titanium.Android.FLAG_SHOW_LIGHTS
        });
        Ti.Android.NotificationManager.notify(1, notification);
        var service = Ti.Android.currentService;
        var serviceIntent = service.getIntent();
        var teststring = serviceIntent.getStringExtra("message");
        Ti.API.info("Extra!: " + teststring);
        Ti.Android.stopService(serviceIntent);
    } else {
        alert("notification count");
        Ti.App.Properties.setInt("notificationCount", 0);
    }
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;