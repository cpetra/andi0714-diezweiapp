var noti = require('notificationController');
var vidPlayer;
var webPageView;

var NavigationController = require('NavigationController'); // use the NavigationController library
var navController = new NavigationController();
Alloy.Globals.navcontroller = navController;
Alloy.Globals.navcontroller.open($.index);


// Check if the device is running iOS 8 or later, before registering for local notifications
if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
    Ti.App.iOS.registerUserNotificationSettings({
	    types: [
            Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
            Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
            Ti.App.iOS.UESR_NOTIFICATION_TYPE_BADGE
        ]
    });
}

if(Ti.Platform.osname != "android" )
	var service = Ti.App.iOS.registerBackgroundService({url:'notificationController.js'});


function doClick(e) {

}

function openVideo(e) {
	vidPlayer = Alloy.createController('vidPlayer');
	vidPlayer.openWindowOut();
}

function openBrowser(e) {
	var webPageView = Alloy.createController('webPage');
	webPageView.openWindowOut();
}

function setNotificationAndroid(){
	if(Ti.Platform.name == 'android') {
		var now = new Date().getTime();
	    var delta = new Date( now + (20 * 1000) );
	    var deltaMS = delta - now;

	    var intent = Ti.Android.createServiceIntent({
	        url : 'ExampleService.js'
	    });
	    intent.putExtra('interval', deltaMS);
	    intent.putExtra('message' , 'This is that little extra');

	    Ti.Android.startService(intent);

	    Titanium.API.info(Ti.Android.currentService);
	}
}

function postNotification(e) {

	if(Ti.Platform.name == 'android') {
		setNotificationAndroid();
	} else {
		Ti.App.fireEvent('postNotificationEvent');
		var notificationLocal = Ti.App.iOS.scheduleLocalNotification({
		    // Create an ID for the notification
		    userInfo: {"id": "foo"},
		    alertBody: "Test? Test?",
		    date: new Date(new Date().getTime() + 3000)
		});

	}
}

Ti.API.debug('update index master');

function cancelNotification(e) {
	Ti.App.fireEvent('cancelNotificationEvent');
}

$.index.addEventListener('open', function(e) {
	if(Ti.Platform.name == 'android') {
		var act = Ti.Android.currentActivity;
		var _intent = act.intent;
		var message = _intent.getStringExtra("ntfId");
		//alert(_intent.data);
		if(_intent.data){
			alert('APPLICATION START FROM LOCAL NOTIFICATION');
		}
		Ti.API.debug('checkout 2 main view');
	}
});


