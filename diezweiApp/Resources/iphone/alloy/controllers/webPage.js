function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "webPage";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.webPage = Ti.UI.createWindow({
        id: "webPage"
    });
    $.__views.webPage && $.addTopLevelView($.__views.webPage);
    $.__views.__alloyId3 = Ti.UI.createLabel({
        text: "WEB Page",
        id: "__alloyId3"
    });
    $.__views.webPage.add($.__views.__alloyId3);
    $.__views.__alloyId4 = Ti.UI.createWebView({
        url: "http://www.bbc.com",
        width: "100%",
        height: "100%",
        id: "__alloyId4"
    });
    $.__views.webPage.add($.__views.__alloyId4);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    exports.openWindowOut = function() {
        Alloy.Globals.navcontroller.open($.webPage);
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;