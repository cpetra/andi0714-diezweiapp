function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function openVideo() {
        vidPlayer = Alloy.createController("vidPlayer");
        vidPlayer.openWindowOut();
    }
    function openBrowser() {
        var webPageView = Alloy.createController("webPage");
        webPageView.openWindowOut();
    }
    function postNotification() {
        Ti.App.fireEvent("postNotificationEvent");
        Ti.App.iOS.scheduleLocalNotification({
            userInfo: {
                id: "foo"
            },
            alertBody: "Test? Test?",
            date: new Date(new Date().getTime() + 3e3)
        });
    }
    function cancelNotification() {
        Ti.App.fireEvent("cancelNotificationEvent");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        modal: "true",
        navBarHidden: "true",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.headerView = Ti.UI.createView({
        top: "0dp",
        height: "60dp",
        width: Ti.Platform.displayCaps.platformWidth,
        backgroundColor: "#cccccc",
        id: "headerView",
        visible: "false"
    });
    $.__views.index.add($.__views.headerView);
    $.__views.__alloyId0 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#ffffff",
        text: "Proof of Concept",
        id: "__alloyId0"
    });
    $.__views.headerView.add($.__views.__alloyId0);
    $.__views.containerBtn = Ti.UI.createView({
        top: "160dp",
        layout: "vertical",
        id: "containerBtn"
    });
    $.__views.index.add($.__views.containerBtn);
    $.__views.btnOpenVideo = Ti.UI.createButton({
        width: "60%",
        title: "Open Video",
        id: "btnOpenVideo"
    });
    $.__views.containerBtn.add($.__views.btnOpenVideo);
    openVideo ? $.__views.btnOpenVideo.addEventListener("click", openVideo) : __defers["$.__views.btnOpenVideo!click!openVideo"] = true;
    $.__views.__alloyId1 = Ti.UI.createButton({
        width: "60%",
        title: "Open Browser",
        id: "__alloyId1"
    });
    $.__views.containerBtn.add($.__views.__alloyId1);
    openBrowser ? $.__views.__alloyId1.addEventListener("click", openBrowser) : __defers["$.__views.__alloyId1!click!openBrowser"] = true;
    $.__views.btnPostNotification = Ti.UI.createButton({
        width: "60%",
        title: "Post Notification",
        id: "btnPostNotification"
    });
    $.__views.containerBtn.add($.__views.btnPostNotification);
    postNotification ? $.__views.btnPostNotification.addEventListener("click", postNotification) : __defers["$.__views.btnPostNotification!click!postNotification"] = true;
    $.__views.btnCancelNotification = Ti.UI.createButton({
        width: "60%",
        title: "Cancel Notification",
        id: "btnCancelNotification"
    });
    $.__views.containerBtn.add($.__views.btnCancelNotification);
    cancelNotification ? $.__views.btnCancelNotification.addEventListener("click", cancelNotification) : __defers["$.__views.btnCancelNotification!click!cancelNotification"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    require("notificationController");
    var vidPlayer;
    var NavigationController = require("NavigationController");
    var navController = new NavigationController();
    Alloy.Globals.navcontroller = navController;
    Alloy.Globals.navcontroller.open($.index);
    true && parseInt(Ti.Platform.version.split(".")[0]) >= 8 && Ti.App.iOS.registerUserNotificationSettings({
        types: [ Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.UESR_NOTIFICATION_TYPE_BADGE ]
    });
    if ("android" != Ti.Platform.osname) {
        Ti.App.iOS.registerBackgroundService({
            url: "notificationController.js"
        });
    }
    $.index.addEventListener("open", function() {
    });
    __defers["$.__views.btnOpenVideo!click!openVideo"] && $.__views.btnOpenVideo.addEventListener("click", openVideo);
    __defers["$.__views.__alloyId1!click!openBrowser"] && $.__views.__alloyId1.addEventListener("click", openBrowser);
    __defers["$.__views.btnPostNotification!click!postNotification"] && $.__views.btnPostNotification.addEventListener("click", postNotification);
    __defers["$.__views.btnCancelNotification!click!cancelNotification"] && $.__views.btnCancelNotification.addEventListener("click", cancelNotification);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;