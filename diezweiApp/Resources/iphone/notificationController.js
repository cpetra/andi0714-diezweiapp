function postNotificationHandler() {
    Titanium.API.info("create new notification");
    "android" == Ti.Platform.osname || (notification = Ti.App.iOS.scheduleLocalNotification({
        alertBody: "Our app made a notification",
        alertAction: "Re-Launch!",
        userInfo: {
            hello: "world"
        },
        sound: "pop.caf",
        date: new Date(new Date().getTime() + 2e4)
    }));
}

function cancelNotificationHandler() {
    "android" == Ti.Platform.osname ? Titanium.Android.NotificationManager.cancelAll() : Ti.App.iOS.cancelAllLocalNotifications();
}

var notification;

var count = 0;

var showMessageCount = 0;

Ti.App.addEventListener("postNotificationEvent", postNotificationHandler);

Ti.App.addEventListener("cancelNotificationEvent", cancelNotificationHandler);

"android" != Ti.Platform.osname && Ti.App.iOS.addEventListener("notification", function(e) {
    var alertIt = Ti.UI.createAlertDialog({
        title: "Notification",
        buttonNames: [ "OK" ],
        message: "NOTIFICATION MESSSAGE"
    });
    void 0 != notification && alertIt.show();
    Ti.API.info("background event received = " + notification);
    Ti.API.info("background event received = " + e);
});