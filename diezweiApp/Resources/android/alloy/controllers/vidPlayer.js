function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vidPlayer";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.vidPlayer = Ti.UI.createWindow({
        navBarHidden: "true",
        id: "vidPlayer"
    });
    $.__views.vidPlayer && $.addTopLevelView($.__views.vidPlayer);
    $.__views.__alloyId2 = Ti.UI.createLabel({
        text: "VIDEO PLAYER",
        id: "__alloyId2"
    });
    $.__views.vidPlayer.add($.__views.__alloyId2);
    $.__views.videoPlayer = Ti.Media.createVideoPlayer({
        id: "videoPlayer",
        ns: Ti.Media,
        top: "2",
        url: "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
        height: "100%",
        width: "100%",
        autoplay: "true"
    });
    $.__views.vidPlayer.add($.__views.videoPlayer);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    exports.openWindowOut = function() {
        Alloy.Globals.navcontroller.open($.vidPlayer);
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;