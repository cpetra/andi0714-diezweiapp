function postNotificationHandler() {
    Titanium.API.info("create new notification");
}

function cancelNotificationHandler() {
    Titanium.Android.NotificationManager.cancelAll();
}

var notification;

var count = 0;

var showMessageCount = 0;

Ti.App.addEventListener("postNotificationEvent", postNotificationHandler);

Ti.App.addEventListener("cancelNotificationEvent", cancelNotificationHandler);