if (Ti.App.Properties.hasProperty("notificationCount")) {
    Ti.App.Properties.removeProperty("notificationCount");
    var activity = Ti.Android.currentActivity;
    var intent = Ti.Android.createIntent({
        action: Ti.Android.ACTION_MAIN,
        data: "tel:2145551234",
        url: "app.js",
        flags: Ti.Android.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Ti.Android.FLAG_ACTIVITY_SINGLE_TOP
    });
    intent.putExtra("ntfId", 1);
    intent.addCategory(Titanium.Android.CATEGORY_LAUNCHER);
    var pending = Ti.Android.createPendingIntent({
        activity: activity,
        intent: intent,
        type: Ti.Android.PENDING_INTENT_FOR_ACTIVITY,
        flags: Ti.Android.FLAG_ACTIVITY_NO_HISTORY
    });
    var notification = Ti.Android.createNotification({
        contentIntent: pending,
        contentTitle: "Android ",
        contentText: "Our app made a notificatiot",
        tickerText: "Notification test",
        when: new Date().getTime(),
        icon: Ti.App.Android.R.drawable.appicon,
        flags: Titanium.Android.ACTION_DEFAULT | Titanium.Android.FLAG_AUTO_CANCEL | Titanium.Android.FLAG_SHOW_LIGHTS
    });
    Ti.Android.NotificationManager.notify(1, notification);
    var service = Ti.Android.currentService;
    var serviceIntent = service.getIntent();
    var teststring = serviceIntent.getStringExtra("message");
    Ti.API.info("Extra!: " + teststring);
    alert("OUR App made a notification");
    Ti.Android.stopService(serviceIntent);
} else Ti.App.Properties.setInt("notificationCount", 0);